package edu.geomant.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.geomant.model.X;

@Repository
public interface XRepository extends CrudRepository<X, Integer>  {

}
