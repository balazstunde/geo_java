package edu.geomant.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.geomant.model.X;
import edu.geomant.service.XService;

@RestController
public class MainController {

	@Autowired
	private XService xService;
	
	@RequestMapping("/hello/{name}")
	public String greeting(@PathVariable String name) {
		return "Hellohello " + name;
	}
	
	@RequestMapping("/xs")
	public List<X> getXs() {
//		List<X> xs = new ArrayList<X>();
//		xs.add(new X(1, "ddddd", "sdjgfwej", "fiuwefkjsdbf", "esdfewfe"));
//		xs.add(new X(2, "eeee", "gfd", "fiuwefkjsdbf", "esdfewfe"));
//		xs.add(new X(3, "fffff", "gdbfgccy", "fiuwefkjsdbf", "esdfewfe"));
//		
//		List<X> xs1 = new ArrayList<X>();
//		xs1.add(new X(1, "aaaaa", "sdjgfwej", "fiuwefkjsdbf", "esdfewfe"));
//		xs1.add(new X(2, "bbbbb", "gfd", "fiuwefkjsdbf", "esdfewfe"));
//		xs1.add(new X(3, "ccccc", "gdbfgccy", "fiuwefkjsdbf", "esdfewfe"));
//		Random rand = new Random();
//		if(rand.nextInt(1000) % 2 == 0) {
//			return xs1;
//		} else {
//			return xs;
//		}
		return xService.getAll();
	}
}
